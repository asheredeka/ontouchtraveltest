<?php

declare(strict_types=1);

namespace App\Form\Type\Joke;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class JokeType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('email', EmailType::class, [
                'label' => 'E-mail',
                'constraints' => [
                    new Email(),
                    new NotBlank()
                ]
            ])
            ->add('category', ChoiceType::class, [
                'label' => 'Category',
                'choices' => array_combine($options['categories'], $options['categories']),
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('send', SubmitType::class, [
                'label' => 'Send'
            ]);
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver
            ->setRequired('categories')
            ->addAllowedTypes('categories', 'array');
    }
}
