<?php

declare(strict_types=1);

namespace App\Controller;

use App\Form\Type\Joke\JokeType;
use App\Service\Joke\Provider\JokeProviderInterface;
use App\Service\Joke\Sender\JokeSenderInterface;
use App\Service\Joke\Storage\JokeStorageInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class HomeController extends AbstractController
{
    /**
     * @param JokeProviderInterface $jokeProvider
     * @param JokeSenderInterface $jokeSender
     * @param JokeStorageInterface $jokeStorage
     * @param Request $request
     * @return Response
     */
    public function index(
        JokeProviderInterface $jokeProvider,
        JokeSenderInterface $jokeSender,
        JokeStorageInterface $jokeStorage,
        Request $request
    ): Response {
        $categories = $jokeProvider->getCategories();
        $jokeForm = $this->createForm(JokeType::class, null, ['categories' => $categories]);
        $jokeForm->handleRequest($request);

        if ($jokeForm->isSubmitted() && $jokeForm->isValid()) {
            $email = $jokeForm->get('email')->getData();
            $category = $jokeForm->get('category')->getData();
            $joke = $jokeProvider->getJoke($category);

            $jokeSender->send($email, $category, $joke);
            $jokeStorage->save($email, $category, $joke);

            $this->addFlash('success', 'Joke has been sent!');

            return $this->redirectToRoute('app_home');
        }

        return $this->render('Home/index.html.twig', [
            'jokeForm' => $jokeForm->createView()
        ]);
    }
}
