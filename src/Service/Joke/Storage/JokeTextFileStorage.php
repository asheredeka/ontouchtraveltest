<?php

declare(strict_types=1);

namespace App\Service\Joke\Storage;

use Symfony\Component\Filesystem\Filesystem;

class JokeTextFileStorage implements JokeStorageInterface
{
    /**
     * @var string
     */
    private $rootDir;

    /**
     * JokeEmailSender constructor.
     * @param string $rootDir
     */
    public function __construct(string $rootDir)
    {
        $this->rootDir = $rootDir;
    }

    /**
     * @inheritDoc
     */
    public function save(string $contact, string $category, string $joke): string
    {
        $fileSystem = new Filesystem();
        $fileName = '/' . $contact . '/' . $category . '/' . uniqid() . '.txt';

        $fileSystem->dumpFile($this->rootDir . $fileName, $joke);

        return $fileName;
    }
}
