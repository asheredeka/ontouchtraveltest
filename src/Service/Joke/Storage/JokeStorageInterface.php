<?php

declare(strict_types=1);

namespace App\Service\Joke\Storage;

interface JokeStorageInterface
{
    /**
     * @param string $contact
     * @param string $category
     * @param string $joke
     * @return string
     */
    public function save(string $contact, string $category, string $joke): string;
}
