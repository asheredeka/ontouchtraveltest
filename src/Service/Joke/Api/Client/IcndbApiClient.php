<?php

declare(strict_types=1);

namespace App\Service\Joke\Api\Client;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class IcndbApiClient implements JokeApiClientInterface
{
    const ICNDB_SUCCESS = 'success';

    /**
     * @var ClientInterface
     */
    private $client;

    /**
     * IcndbApiClient constructor.
     * @param ClientInterface $client
     */
    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @inheritDoc
     */
    public function getCategories(): Response
    {
        return $this->request('/categories');
    }

    /**
     * @inheritDoc
     */
    public function getJoke(string $category): Response
    {
        return $this->request('/jokes/random', [
            'query' => [
                'limitTo' => "[$category]"
            ]
        ]);
    }

    /**
     * @param string $uri
     * @param array $options
     * @return Response
     */
    private function request(string $uri, array $options = []): Response
    {
        try {
            $response = $this->client->request('get', $uri, $options);

            return $this->parseResponse($response);
        } catch (ClientException $exception) {
            $response = $exception->getResponse();

            return new JsonResponse([
                'message' => $exception->getMessage()
            ], $response->getStatusCode(), $response->getHeaders());
        } catch (GuzzleException $exception) {
            return new JsonResponse([
                'message' => $exception->getMessage()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @param ResponseInterface $response
     * @return Response
     */
    private function parseResponse(ResponseInterface $response): Response
    {
        $content = $response->getBody()->getContents();
        $data = json_decode($content, true);
        $responseType = $data['type'] ?? null;

        if ($responseType === self::ICNDB_SUCCESS) {
            return new JsonResponse(['data' => $data['value']], Response::HTTP_OK);
        }

        if (!$data) {
            return new JsonResponse([
                'message' => $content
            ], Response::HTTP_BAD_REQUEST);
        }

        return new JsonResponse(['messages' => $data['value']], Response::HTTP_BAD_REQUEST);
    }
}
