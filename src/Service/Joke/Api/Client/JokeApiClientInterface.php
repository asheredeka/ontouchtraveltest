<?php

declare(strict_types=1);

namespace App\Service\Joke\Api\Client;

use Symfony\Component\HttpFoundation\Response;

interface JokeApiClientInterface
{
    /**
     * @return Response
     */
    public function getCategories(): Response;

    /**
     * @param string $category
     * @return Response
     */
    public function getJoke(string $category): Response;
}
