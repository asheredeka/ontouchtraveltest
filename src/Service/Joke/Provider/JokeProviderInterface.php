<?php

declare(strict_types=1);

namespace App\Service\Joke\Provider;

interface JokeProviderInterface
{
    const JOKE_PROVIDER_ERROR_GETTING_CATEGORIES = 'Error getting categories';
    const JOKE_PROVIDER_ERROR_GETTING_JOKE = 'Error getting joke';

    /**
     * @return string[]
     */
    public function getCategories(): array;

    /**
     * @param string $category
     * @return string
     */
    public function getJoke(string $category): string;
}
