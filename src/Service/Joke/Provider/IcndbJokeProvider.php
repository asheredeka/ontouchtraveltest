<?php

declare(strict_types=1);

namespace App\Service\Joke\Provider;

use App\Service\Joke\Api\Client\JokeApiClientInterface;
use Symfony\Component\HttpFoundation\Response;

class IcndbJokeProvider implements JokeProviderInterface
{
    /**
     * @var JokeApiClientInterface
     */
    private $jokeApiClient;

    /**
     * IcndbJokeProvider constructor.
     * @param JokeApiClientInterface $jokeApiClient
     */
    public function __construct(JokeApiClientInterface $jokeApiClient)
    {
        $this->jokeApiClient = $jokeApiClient;
    }

    /**
     * @inheritDoc
     * @throws JokeProviderException
     * @todo Кэширование категорий
     */
    public function getCategories(): array
    {
        $response = $this->jokeApiClient->getCategories();
        $json = $this->responseToJson($response);

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            throw new JokeProviderException($json['message'] ?? JokeProviderInterface::JOKE_PROVIDER_ERROR_GETTING_CATEGORIES);
        }

        $categories = $json['data'] ?? [];

        if (count($categories) === 0) {
            throw new JokeProviderException(JokeProviderInterface::JOKE_PROVIDER_ERROR_GETTING_CATEGORIES);
        }

        return $categories;
    }

    /**
     * @inheritDoc
     * @throws JokeProviderException
     */
    public function getJoke(string $category): string
    {
        $response = $this->jokeApiClient->getJoke($category);
        $json = $this->responseToJson($response);

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            throw new JokeProviderException($json['message'] ?? JokeProviderInterface::JOKE_PROVIDER_ERROR_GETTING_JOKE);
        }

        $joke = $json['data']['joke'] ?? null;

        if ($joke === null) {
            throw new JokeProviderException(JokeProviderInterface::JOKE_PROVIDER_ERROR_GETTING_JOKE);
        }

        return (string)$joke;
    }

    /**
     * @param Response $response
     * @return array
     */
    private function responseToJson(Response $response): array
    {
        return json_decode($response->getContent(), true);
    }
}
