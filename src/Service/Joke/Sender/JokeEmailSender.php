<?php

declare(strict_types=1);

namespace App\Service\Joke\Sender;

use Swift_Mailer;
use Swift_Message;

class JokeEmailSender implements JokeSenderInterface
{
    /**
     * @var Swift_Mailer
     */
    private $mailer;

    /**
     * @var string
     */
    private $fromEmail;

    /**
     * @var string
     */
    private $fromName;

    /**
     * JokeEmailSender constructor.
     * @param Swift_Mailer $mailer
     * @param string $fromEmail
     * @param string $fromName
     */
    public function __construct(Swift_Mailer $mailer, string $fromEmail, string $fromName)
    {
        $this->mailer = $mailer;
        $this->fromName = $fromName;
        $this->fromEmail = $fromEmail;
    }

    /**
     * @inheritDoc
     */
    public function send(string $contact, string $category, string $joke): bool
    {
        $subject = sprintf(JokeSenderInterface::JOKE_SENDER_SUBJECT_TEMPLATE, $category);

        $message = new Swift_Message($subject);
        $message->setTo($contact);
        $message->setFrom($this->fromEmail, $this->fromName);
        $message->setBody($joke, 'text/plain');

        if ($this->mailer->send($message) === 0) {
            return false;
        }

        return true;
    }
}
