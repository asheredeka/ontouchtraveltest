<?php

declare(strict_types=1);

namespace App\Service\Joke\Sender;

interface JokeSenderInterface
{
    const JOKE_SENDER_SUBJECT_TEMPLATE = 'Случайная шутка из %s';

    /**
     * @param string $contact
     * @param string $category
     * @param string $joke
     * @return bool
     */
    public function send(string $contact, string $category, string $joke): bool;
}
