<?php

declare(strict_types=1);

namespace App\Tests\Service\Joke\Sender;

use App\Service\Joke\Sender\JokeEmailSender;
use Swift_Mailer;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

class JokeEmailSenderTest extends TestCase
{
    public function testFailSend()
    {
        $mailer = $this->createMock(Swift_Mailer::class);

        $mailer
            ->expects($this->once())
            ->method('send')
            ->willReturn(0);

        $jokeEmailSender = new JokeEmailSender($mailer, 'from@email.com', 'from');
        $result = $jokeEmailSender->send('email@email.com', 'category', 'joke');

        $this->assertFalse($result);
    }

    public function testSend()
    {
        $mailer = $this->createMock(Swift_Mailer::class);

        $mailer
            ->expects($this->once())
            ->method('send')
            ->willReturn(1);

        $jokeEmailSender = new JokeEmailSender($mailer, 'from@email.com', 'from');
        $result = $jokeEmailSender->send('email@email.com', 'category', 'joke');

        $this->assertTrue($result);
    }
}
