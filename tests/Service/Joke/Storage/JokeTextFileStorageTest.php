<?php

declare(strict_types=1);

namespace App\Tests\Service\Joke\Storage;

use App\Service\Joke\Storage\JokeTextFileStorage;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;
use Symfony\Component\Filesystem\Filesystem;

class JokeTextFileStorageTest extends TestCase
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var string
     */
    private $rootDir;

    protected function setUp(): void
    {
        $this->filesystem = new Filesystem();
        $this->rootDir = sys_get_temp_dir().'/'.microtime(true).'.'.mt_rand();
        mkdir($this->rootDir, 0777, true);
    }

    protected function tearDown(): void
    {
        $this->filesystem->remove($this->rootDir);
    }

    public function testSave()
    {
        $jokeTextFileStorage = new JokeTextFileStorage($this->rootDir);

        $fileName = $jokeTextFileStorage->save('email@email.com', 'category', 'joke');

        $this->assertTrue(is_file($this->rootDir . $fileName));
        $this->assertEquals('joke', file_get_contents($this->rootDir . $fileName));
    }
}
