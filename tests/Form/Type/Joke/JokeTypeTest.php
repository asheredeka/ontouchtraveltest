<?php

declare(strict_types=1);

namespace App\Tests\Form\Type\Joke;

use App\Form\Type\Joke\JokeType;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\OptionsResolver\Exception\MissingOptionsException;

class JokeTypeTest extends WebTestCase
{
    public function testFailFormCreate()
    {
        static::bootKernel();

        $this->expectException(MissingOptionsException::class);

        self::$container->get('form.factory')->create(JokeType::class);
    }

    public function testFormCreate()
    {
        static::bootKernel();

        $form = self::$container->get('form.factory')->create(JokeType::class, null, [
            'categories' => ['category']
        ]);

        $view = $form->createView();

        $this->assertArrayHasKey('email', $view);
        $this->assertArrayHasKey('category', $view);
        $this->assertArrayHasKey('send', $view);
    }

    public function testWrongEmailFormSubmit()
    {
        static::bootKernel();

        $form = self::$container->get('form.factory')
            ->create(JokeType::class, null, [
                'categories' => ['category']
            ])
            ->submit([
                'email' => 'email',
                'category' => 'category',
                '_token' => self::$container->get('security.csrf.token_manager')->getToken('joke')->getValue()
            ])
        ;

        $this->assertFalse($form->isValid());
    }

    public function testNoEmailFormSubmit()
    {
        static::bootKernel();

        $form = self::$container->get('form.factory')
            ->create(JokeType::class, null, [
                'categories' => ['category']
            ])
            ->submit([
                'category' => 'category',
                '_token' => self::$container->get('security.csrf.token_manager')->getToken('joke')->getValue()
            ])
        ;

        $this->assertFalse($form->isValid());
    }

    public function testWrongCategoryFormSubmit()
    {
        static::bootKernel();

        $form = self::$container->get('form.factory')
            ->create(JokeType::class, null, [
                'categories' => ['category']
            ])
            ->submit([
                'email' => 'email',
                'category' => 'wrong_category',
                '_token' => self::$container->get('security.csrf.token_manager')->getToken('joke')->getValue()
            ])
        ;

        $this->assertFalse($form->isValid());
    }

    public function testNoCategoryFormSubmit()
    {
        static::bootKernel();

        $form = self::$container->get('form.factory')
            ->create(JokeType::class, null, [
                'categories' => ['category']
            ])
            ->submit([
                'email' => 'email',
                '_token' => self::$container->get('security.csrf.token_manager')->getToken('joke')->getValue()
            ])
        ;

        $this->assertFalse($form->isValid());
    }

    public function testFormSubmit()
    {
        static::bootKernel();

        $form = self::$container->get('form.factory')
            ->create(JokeType::class, null, [
                'categories' => ['category']
            ])
            ->submit([
                'email' => 'email@email.com',
                'category' => 'category',
                '_token' => self::$container->get('security.csrf.token_manager')->getToken('joke')->getValue()
            ])
        ;

        $this->assertTrue($form->isValid());

        $this->assertEquals('email@email.com', $form->get('email')->getData());
        $this->assertEquals('category', $form->get('category')->getData());
    }
}
