<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use App\Service\Joke\Provider\JokeProviderInterface;
use App\Service\Joke\Sender\JokeSenderInterface;
use App\Service\Joke\Storage\JokeStorageInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Swift_Message;

class HomeControllerTest extends WebTestCase
{
    public function testIndex(): void
    {
        $client = static::createClient();

        $jokeProvider = $this->createMock(JokeProviderInterface::class);

        $jokeProvider
            ->expects($this->once())
            ->method('getCategories')
            ->willReturn(['category']);

        self::$container->set('test.' . JokeProviderInterface::class, $jokeProvider);

        $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testFailSubmitIndex(): void
    {
        $client = static::createClient();

        $jokeProvider = $this->createMock(JokeProviderInterface::class);
        $jokeSender = $this->createMock(JokeSenderInterface::class);
        $jokeStorage = $this->createMock(JokeStorageInterface::class);

        $jokeProvider
            ->expects($this->once())
            ->method('getCategories')
            ->willReturn(['category']);

        $jokeProvider
            ->expects($this->never())
            ->method('getJoke');

        $jokeSender
            ->expects($this->never())
            ->method('send');

        $jokeStorage
            ->expects($this->never())
            ->method('save');

        self::$container->set('test.' . JokeProviderInterface::class, $jokeProvider);
        self::$container->set('test.' . JokeSenderInterface::class, $jokeSender);
        self::$container->set('test.' . JokeStorageInterface::class, $jokeStorage);

        $client->request('POST', '/', [
            'joke' => [
                'email' => 'email@email.com',
                'category' => 'category'
            ]
        ]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testSubmitIndex(): void
    {
        $client = static::createClient();

        $client->enableProfiler();

        $jokeProvider = $this->createMock(JokeProviderInterface::class);
        $jokeStorage = $this->createMock(JokeStorageInterface::class);

        $jokeProvider
            ->expects($this->once())
            ->method('getCategories')
            ->willReturn(['category']);

        $jokeProvider
            ->expects($this->once())
            ->method('getJoke')
            ->with('category')
            ->willReturn('joke');

        $jokeStorage
            ->expects($this->once())
            ->method('save')
            ->with('email@email.com', 'category', 'joke');

        self::$container->set('test.' . JokeProviderInterface::class, $jokeProvider);
        self::$container->set('test.' . JokeStorageInterface::class, $jokeStorage);

        $client->request('POST', '/', [
            'joke' => [
                'email' => 'email@email.com',
                'category' => 'category',
                '_token' => self::$container->get('security.csrf.token_manager')->getToken('joke')->getValue()
            ]
        ]);

        $mailCollector = $client->getProfile()->getCollector('swiftmailer');

        $collectedMessages = $mailCollector->getMessages();
        /** @var Swift_Message $message */
        $message = $collectedMessages[0];

        $this->assertEquals(302, $client->getResponse()->getStatusCode());

        $this->assertInstanceOf(Swift_Message::class, $message);
        $this->assertSame('Случайная шутка из category', $message->getSubject());
        $this->assertSame('email@email.com', key($message->getTo()));
        $this->assertSame('joke', $message->getBody());
    }
}
